#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# convert a list of integers into a list of strings 
def convert_int_to_str(digits):     
    res = list(map(str, digits))
    return(res) 


# In[ ]:


# add zero in the first 9 elements 
def add_zero(values):
    res = ["0" + v for v in values]
    return(res) 


# In[ ]:


# replace the first 9 elements 
def change_subset_str_values(values):
    res = add_zero(values[0:9])
    new_values = res + values[9:]    
    return(new_values) 


# In[ ]:


# get days range
def get_days_range(start, end):
    digits_range = range(1,32)
    strings_range = convert_int_to_str(digits_range)
    days_range = change_subset_str_values(strings_range)
    return(days_range)

