{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction to [NumPy](https://docs.scipy.org/doc/numpy-1.17.0/reference/)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. n-dimensional array\n",
    "\n",
    "The core data structure in NumPy is the **ndarray** or **n-dimensional array**. \n",
    "\n",
    "**array** describes a collection of elements, similar to a list. The word **n-dimensional** refers to the fact that ndarrays can have one or more dimensions.\n",
    "\n",
    "To use the NumPy library, we first need to import it into our Python environment. NumPy is commonly imported using the alias `np`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then, we can directly convert a list to an ndarray using the `numpy.array()` [constructor](https://docs.scipy.org/doc/numpy/reference/generated/numpy.array.html). To create a 1D ndarray, we can pass in a single list:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "data_ndarray = np.array([5, 10, 15, 20])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We used the syntax `np.array()` instead of `numpy.array()` because of our `import numpy as np` code. \n",
    "\n",
    "Let's practice creating 1D ndarrays next."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Import `numpy` and assign to the alias `np`.\n",
    "* Create a NumPy ndarray from the list `[10, 20, 30, 40]`. \n",
    "* Assign the result to the variable `data_ndarray`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[10 20 30 40]\n"
     ]
    }
   ],
   "source": [
    "import numpy as np\n",
    "\n",
    "data_ndarray = np.array([10,20,30, 40])\n",
    "\n",
    "print(data_ndarray)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, let's explore why ```ndarrays``` and the NumPy library make it easier to manipulate and analyze data.\n",
    "\n",
    "To represent small data sets we can use **lists of lists**, but they aren't very good for working with larger data sets.\n",
    "\n",
    "Let's look at an example where we have two columns of data. Each row contains two numbers we wish to add together. Using just Python, we could use a list of lists structure to store our data, and use **for loops** to iterate over that data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[11, 4, 11, 5, 10, 13, 8, 12]"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# list of list representation\n",
    "my_numbers = [\n",
    "    [6, 5],\n",
    "    [1, 3],\n",
    "    [5, 6],\n",
    "    [1, 4],\n",
    "    [3, 7],\n",
    "    [5, 8],\n",
    "    [3, 5],\n",
    "    [8, 4]\n",
    "]\n",
    "\n",
    "# sum each row\n",
    "sums = []\n",
    "\n",
    "for row in my_numbers:\n",
    "  row_sum = row[0] + row[1]\n",
    "  sums.append(row_sum)\n",
    "    \n",
    "sums"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It's often useful to know the number of rows and columns in an ndarray. We can use the `ndarray.shape` attribute."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(2, 3)\n"
     ]
    }
   ],
   "source": [
    "data_ndarray = np.array([[5, 10, 15], [20, 25, 30]])\n",
    "\n",
    "print(data_ndarray.shape)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(2, 3)"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "np.array([[5, 10, 15], [20, 25, 30]]).shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The data type returned is called a tuple. `Tuples` are very similar to Python lists, but `can't be modified`.\n",
    "\n",
    "The output gives us a few important pieces of information:\n",
    "\n",
    "* The first number tells us that there are 2 rows in `data_ndarray`.\n",
    "* The second number tells us that there are 3 columns in `data_ndarray`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, let's look at a comparison between working with `ndarrays` and `list of lists` to select one or more rows of data (e.g., 5 rows, 5 columns)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1, 3, 2, 3, 4]\n",
      "[[5, 6, 7, 8, 9], [1, 4, 3, 2, 1], [3, 7, 5, 6, 7]]\n"
     ]
    }
   ],
   "source": [
    "data_lol = [\n",
    "    [6, 5, 4, 5, 6],\n",
    "    [1, 3, 2, 3, 4],\n",
    "    [5, 6, 7, 8, 9],\n",
    "    [1, 4, 3, 2, 1],\n",
    "    [3, 7, 5, 6, 7],\n",
    "]\n",
    "\n",
    "# selecting a single row to get a 1D ndarray by using a list of lists method\n",
    "sel_lol = data_lol[1] # 1 is the second row\n",
    "print(sel_lol)\n",
    "# selecting a single row to get a 2D ndarray by using a list of lists method\n",
    "sel_lol = data_lol[2:] # from the third row\n",
    "print(sel_lol)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1 3 2 3 4]\n",
      "[[5 6 7 8 9]\n",
      " [1 4 3 2 1]\n",
      " [3 7 5 6 7]]\n"
     ]
    }
   ],
   "source": [
    "data_np = np.array([[6, 5, 4, 5, 6],\n",
    "    [1, 3, 2, 3, 4],\n",
    "    [5, 6, 7, 8, 9],\n",
    "    [1, 4, 3, 2, 1],\n",
    "    [3, 7, 5, 6, 7]])\n",
    "\n",
    "# selecting a single row to get a 1D ndarray by using a NumPy method\n",
    "sel_np = data_np[1]\n",
    "print(sel_np)\n",
    "\n",
    "# selecting a single row to get a 2D ndarray by using a NumPy method\n",
    "sel_np = data_np[2:]\n",
    "print(sel_np)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For any 2D array, the full syntax for selcting data is:    \n",
    "\n",
    "```\n",
    "ndarray[row_index, column_index]\n",
    "```\n",
    "\n",
    "or if you want to select all  columns for a given set of rows:\n",
    "\n",
    "```\n",
    "ndarray[row_index]\n",
    "```\n",
    "\n",
    "Where `row_index` defines the location along the row axis and `column_index` defines the location along the column axis.\n",
    "\n",
    "Like lists, array slicing is from the first specified index up to — but not including — the second specified index. For example, to select the items at index `1`, `2`, and `3`, we'd need to use the slice `[1:4]`.\n",
    "\n",
    "This is how we select a single item from a 2D ndarray (e.g., 5 rows, 5 columns)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "3\n"
     ]
    }
   ],
   "source": [
    "# selecting a single item to get a single Python object by using a list of lists method\n",
    "sel_lol = data_lol[1][3]\n",
    "print(sel_lol)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "3\n"
     ]
    }
   ],
   "source": [
    "# selecting a single item to get a single Python object by using a NumPy method\n",
    "sel_np = data_np[1, 3]\n",
    "print(sel_lol)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is how we select all columns for a given row."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1, 3, 2, 3, 4]\n"
     ]
    }
   ],
   "source": [
    "# selecting all columns for a given row by using a list of lists method\n",
    "sel_lol = data_lol[1]\n",
    "print(sel_lol)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1, 3, 2, 3, 4]\n"
     ]
    }
   ],
   "source": [
    "# selecting all columns for a given row by using a NumPy method\n",
    "sel_np = data_np[1]\n",
    "print(sel_lol)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With a list of lists, we use two separate pairs of square brackets back-to-back. With a NumPy ndarray, we use a single pair of brackets with comma-separated row and column locations.\n",
    "\n",
    "Let's continue by learning how to select one or more columns of data (e.g., 5 rows, 5 columns)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[5, 3, 8, 2, 6]\n",
      "[[5, 4], [3, 2], [6, 7], [4, 3], [7, 5]]\n",
      "[[5, 5, 6], [3, 3, 4], [6, 8, 9], [4, 2, 1], [7, 6, 7]]\n"
     ]
    }
   ],
   "source": [
    "# selecting a single column to produce a 1D ndarray by using a list of lists method\n",
    "\n",
    "sel_lol1 = []\n",
    "\n",
    "for row in data_lol:\n",
    "  col4 = row[3]\n",
    "  sel_lol1.append(col4)\n",
    "\n",
    "print(sel_lol1)\n",
    "\n",
    "# selecting multiple columns to produce a 2D ndarray by using a list of lists method\n",
    "\n",
    "sel_lol2 = []\n",
    "for row in data_lol:\n",
    "  col23 = row[1:3]\n",
    "  sel_lol2.append(col23)\n",
    "\n",
    "print(sel_lol2)\n",
    "    \n",
    "# selecting multiple specific columns\n",
    "\n",
    "sel_lol3 = []\n",
    "\n",
    "for row in data_lol:\n",
    "  cols = [row[1], row[3], row[4]]\n",
    "  sel_lol3.append(cols)\n",
    "\n",
    "print(sel_lol3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[5 3 8 2 6]\n",
      "[[5 4]\n",
      " [3 2]\n",
      " [6 7]\n",
      " [4 3]\n",
      " [7 5]]\n",
      "[[5 5 6]\n",
      " [3 3 4]\n",
      " [6 8 9]\n",
      " [4 2 1]\n",
      " [7 6 7]]\n"
     ]
    }
   ],
   "source": [
    "# selecting a single column to produce a 1D ndarray by using a NumPy method\n",
    "sel_np1 = data_np[:,3]\n",
    "print(sel_np1)\n",
    "\n",
    "# selecting multiple columns to produce a 2D ndarray by using a NumPy method\n",
    "sel_np2 = data_np[:,1:3]\n",
    "print(sel_np2)\n",
    "\n",
    "# slecting multiple specific columns\n",
    "cols = [1, 3, 4]\n",
    "sel_np3 = data_np[:,cols]\n",
    "print(sel_np3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With a list of lists, we need to use a for loop to extract specific column(s) and append them back to a new list. With ndarrays, the process is much simpler. We again use single brackets with comma-separated row and column locations, but we use a colon (`:`) for the row locations, which gives us all of the rows.\n",
    "\n",
    "If we want to select a partial 1D slice of a row or column, we can combine a single value for one dimension with a slice for the other dimension (e.g., 5 rows, 5 columns)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[6, 7, 8]\n",
      "[4, 9, 1, 7]\n"
     ]
    }
   ],
   "source": [
    "# selecting a 1D slice (row) to produce a 1D ndarray by using a list of lists method\n",
    "sel_lol1 = data_lol[2][1:4]\n",
    "print(sel_lol1)\n",
    "\n",
    "# selecting a 1D slice (column) to produce a 1D ndarray by using a list of lists method\n",
    "sel_lol2 = []\n",
    "rows = data_lol[1:]\n",
    "for row in rows:\n",
    "  col5 = row[4]\n",
    "  sel_lol2.append(col5)\n",
    "print(sel_lol2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[6 7 8]\n",
      "[4 9 1 7]\n"
     ]
    }
   ],
   "source": [
    "# selecting a 1D slice (row) to produce a 2D ndarray by using a NumPy method\n",
    "sel_np1 = data_np[2,1:4]\n",
    "print(sel_np1)\n",
    "\n",
    "# selecting a 1D slice (row) to produce a 2D ndarray by using a NumPy method\n",
    "sel_np2 = data_np[1:,4]\n",
    "print(sel_np2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lastly, if we want to select a 2D slice, we can use slices for both dimensions (e.g., 5 rows, 5 columns)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[1, 3, 2], [5, 6, 7], [1, 4, 3]]\n"
     ]
    }
   ],
   "source": [
    "# selecting a 2D slice to produce a 2D ndarray by using a list of lists method\n",
    "\n",
    "sel_lol = []\n",
    "rows = data_lol[1:4]\n",
    "for row in rows:\n",
    "  new_row = row[:3]\n",
    "  sel_lol.append(new_row)\n",
    "print(sel_lol)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[1 3 2]\n",
      " [5 6 7]\n",
      " [1 4 3]]\n"
     ]
    }
   ],
   "source": [
    "# selecting a 2D slice to produce a 2D ndarray by using a NumPy method\n",
    "\n",
    "sel_np = data_np[1:4,:3]\n",
    "print(sel_np)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "NumPy ndarrays allow us to select data much more easily. Beyond this, the selection we make is a lot faster when working with **vectorized operations** because the operations are applied to multiple data points at once.\n",
    "\n",
    "When we first talked about vectorized operations, we used the example of adding two columns of data. With data in a list of lists, we'd have to construct a for-loop and add each pair of values from each row individually:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[11, 4, 11, 5, 10, 13, 8, 12]"
      ]
     },
     "execution_count": 19,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# list of list representation\n",
    "my_numbers = [\n",
    "    [6, 5],\n",
    "    [1, 3],\n",
    "    [5, 6],\n",
    "    [1, 4],\n",
    "    [3, 7],\n",
    "    [5, 8],\n",
    "    [3, 5],\n",
    "    [8, 4]\n",
    "]\n",
    "\n",
    "# sum each row\n",
    "sums = []\n",
    "\n",
    "for row in my_numbers:\n",
    "  row_sum = row[0] + row[1]\n",
    "  sums.append(row_sum)\n",
    "\n",
    "sums"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Vectorized operations make our code **faster** and **easier to execute**. Here's how we would perform the same task above with vectorized operations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[11  4 11  5 10 13  8 12]\n",
      "[11  4 11  5 10 13  8 12]\n"
     ]
    }
   ],
   "source": [
    "# convert the list of lists to an ndarray\n",
    "my_numbers = np.array(my_numbers)\n",
    "\n",
    "# select each of the columns - the result of each will be a 1D ndarray\n",
    "col1 = my_numbers[:,0]\n",
    "col2 = my_numbers[:,1]\n",
    "\n",
    "# add the two columns\n",
    "sums = col1 + col2\n",
    "print(sums)\n",
    "\n",
    "# We could simplify this further if we wanted to:\n",
    "sums = my_numbers[:,0] + my_numbers[:,1]\n",
    "print(sums)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here are some key observations about this code:\n",
    "\n",
    "* When we selected each column, we used the syntax `ndarray[:,c]` where `c` is the column index we wanted to select. Like we saw in the previous screen, the colon selects all rows.\n",
    "* To add the two 1D ndarrays, `col1` and `col2`, we simply use the addition operator (`+`) between them.\n",
    "\n",
    "The result of adding two 1D ndarrays is a 1D ndarray of the same shape (or dimensions) as the original. What we just did, adding two vectors together, is called vector addition."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the last screen, we used vector addition to add two columns, or vectors, together. We can actually use any of the standard [Python numeric operators](https://docs.python.org/3/library/stdtypes.html#numeric-types-int-float-complex) with vectors, including:\n",
    "\n",
    "* `vector_a + vector_b` - Addition\n",
    "* `vector_a - vector_b` - Subtraction\n",
    "* `vector_a * vector_b` - Multiplication (this is unrelated to the vector multiplication used in linear algebra).\n",
    "* `vector_a / vector_b` - Division\n",
    "\n",
    "When we perform these operations on two 1D vectors, both vectors must have the same shape."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Numpy ndarrays have methods for many different calculations. \n",
    "\n",
    "* `ndarray.min()` [to calculate the minimum value](https://docs.scipy.org/doc/numpy-1.17.0/reference/generated/numpy.ndarray.min.html#numpy.ndarray.min)\n",
    "* `ndarray.max()` [to calculate the maximum value](https://docs.scipy.org/doc/numpy-1.17.0/reference/generated/numpy.ndarray.max.html#numpy.ndarray.max)\n",
    "* `ndarray.mean()` [to calculate the mean or average value](https://docs.scipy.org/doc/numpy-1.14.0/reference/generated/numpy.ndarray.mean.html#numpy.ndarray.mean)\n",
    "* `ndarray.sum()` [to calculate the sum of the values](https://docs.scipy.org/doc/numpy-1.17.0/reference/generated/numpy.ndarray.sum.html#numpy.ndarray.sum)\n",
    "\n",
    "You can see the full list of ndarray methods in the [NumPy ndarray documentation](https://docs.scipy.org/doc/numpy-1.17.0/reference/arrays.ndarray.html#calculation).\n",
    "\n",
    "It's important to get comfortable with the documentation because it's not possible to remember the syntax for every variation of every data science library. However, if you remember what is possible and can read the documentation, you'll always be able to refamiliarize yourself with it."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before we look at other array methods, let's review the difference between methods and functions. \n",
    "\n",
    "**Functions** act as stand alone segments of code that usually take an input, perform some processing, and return some output. For example, we can use the `len()` function to calculate the length of a list or the number of characters in a string."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "3\n"
     ]
    }
   ],
   "source": [
    "my_list = [21,14,91]\n",
    "\n",
    "print(len(my_list))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "9\n"
     ]
    }
   ],
   "source": [
    "my_string = 'Dataquest'\n",
    "\n",
    "print(len(my_string))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we'll calculate statistics for 2D ndarrays. If we use the `ndarray.max()` method on a 2D ndarray without any additional parameters, it will return a single value, just like with a 1D array.\n",
    "\n",
    "Array method without axis parameter `ndarray.max()`\n",
    "\n",
    "But what if we wanted to find the maximum value of each row? We'd need to use the `axis` parameter and specify a value of `1` to indicate we want to calculate the maximum value for each row.\n",
    "\n",
    "Array method without axis 1 `ndarray.max(axis=1)`\n",
    "\n",
    "If we want to find the maximum value of each column, we'd use an `axis` value of `0:`\n",
    "\n",
    "Array method without axis 1 `ndarray.max(axis=0)`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Boolean indexing\n",
    "\n",
    "We have learned how to index — or select — data from ndarrays. Now, we're going to focus on the **boolean array**. A boolean array, as the name suggests, is an array of boolean values. Boolean arrays are sometimes called **boolean vectors** or **boolean masks**.\n",
    "\n",
    "You may recall that the boolean (or `bool`) type is a built-in Python type that can be one of two unique values:\n",
    "\n",
    "* `True`\n",
    "* `False`\n",
    "\n",
    "You may also remember that we've used boolean values when working with Python [comparison](https://docs.python.org/3.7/library/stdtypes.html#comparisons) operators like `==` (equal), `>` (greater than), `<` (less than), `!=` (not equal). Below are a couple examples of simple boolean operations:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "True\n"
     ]
    }
   ],
   "source": [
    "print(type(3.5) == float)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "False\n"
     ]
    }
   ],
   "source": [
    "print(5 > 6)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When we explored vector math in the first mission, we learned that an operation between a ndarray and a single value results in a new ndarray:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[12 14 16 18]\n"
     ]
    }
   ],
   "source": [
    "print(np.array([2,4,6,8]) + 10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `+ 10` operation is applied to each value in the array.\n",
    "\n",
    "Now, let's look at what happens when we perform a boolean operation between an ndarray and a single value:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[ True  True False False]\n"
     ]
    }
   ],
   "source": [
    "np.array([2,4,6,8]) # ndarray\n",
    "np.array([2,4,6,8]) < 5 # vectorized boolean operation         \n",
    "print(np.array([2,4,6,8]) < 5) # boolean ndarray"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A similar pattern occurs – each value in the array is compared to five. If the value is less than five, `True` is returned. Otherwise, `False` is returned.\n",
    "\n",
    "Let's practice using vectorized boolean operations to create some boolean arrays."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Evaluate whether the elements in array `a` are less than `3`. Assign the result to `a_bool`.\n",
    "  \n",
    "a = np.array([1, 2, 3, 4, 5])\n",
    "\n",
    "# Evaluate whether the elements in array `b` are equal to `\"blue\"`. Assign the result to `b_bool`.\n",
    "  \n",
    "b = np.array([\"blue\", \"blue\", \"red\", \"blue\"])\n",
    "\n",
    "# Evaluate whether the elements in array `c` are greater than `100`. Assign the result to `c_bool`.\n",
    "  \n",
    "c = np.array([80.0, 103.4, 96.9, 200.3])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Remember that `=` (one equals sign) is used for assignment, but `==` (two equals signs) is used to test for equality with a boolean operator."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = np.array([1, 2, 3, 4, 5])\n",
    "b = np.array([\"blue\", \"blue\", \"red\", \"blue\"])\n",
    "c = np.array([80.0, 103.4, 96.9, 200.3])\n",
    "\n",
    "a_bool = a<3\n",
    "\n",
    "b_bool = b == 'blue'\n",
    "\n",
    "c_bool = c > 100"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We learned how to create boolean arrays using vectorized boolean operations. Next, we'll learn how to index (or select) using boolean arrays, known as **boolean indexing**. Let's use one of the examples from the previous screen."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[ 80.  103.4  96.9 200.3]\n",
      "[False  True False  True]\n"
     ]
    }
   ],
   "source": [
    "c = np.array([80.0, 103.4, 96.9, 200.3])\n",
    "print(c)\n",
    "c_bool = c > 100\n",
    "print(c_bool)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To index using our new boolean array, we simply insert it in the square brackets, just like we would do with our other selection techniques:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[103.4 200.3]\n"
     ]
    }
   ],
   "source": [
    "result = c[c_bool]\n",
    "print(result)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The boolean array acts as a filter, so that the values corresponding to `True` become part of the result and the values corresponding to `False` are removed."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When working with 2D ndarrays, you can use boolean indexing in combination with any of the indexing methods we learned in the previous mission. The only limitation is that the boolean array must have the same length as the dimension you're indexing. Let's look at some examples:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[ 1  2  3]\n",
      " [ 4  5  6]\n",
      " [ 7  8  9]\n",
      " [10 11 12]]\n"
     ]
    }
   ],
   "source": [
    "arr = np.array([\n",
    "    [1, 2, 3],\n",
    "    [4, 5, 6],\n",
    "    [7, 8, 9],\n",
    "    [10, 11, 12]\n",
    "])\n",
    "\n",
    "print(arr)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 32,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[ 1  2  3]\n",
      " [ 7  8  9]\n",
      " [10 11 12]]\n"
     ]
    }
   ],
   "source": [
    "bool_1 = [True, False, True, True] \n",
    "\n",
    "print(arr[bool_1]) # it selects the 1st, 3rd, and 5th rows"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 33,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[ 2  3]\n",
      " [ 5  6]\n",
      " [ 8  9]\n",
      " [11 12]]\n"
     ]
    }
   ],
   "source": [
    "bool_2 = [False, True, True]\n",
    "\n",
    "print(arr[:,bool_2]) # It selects the 2nd and 3rd columns\n",
    "\n",
    "#bool_2's shape (3) is the same as the shape of arr's second axis (3) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Because a boolean array contains no information about how it was created, we can use a boolean array made from just one column of our array to index the whole array."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So far, we've learned how to retrieve data from ndarrays. Next, we'll use the same indexing techniques we've already learned to modify values within an ndarray. The syntax we'll use (in pseudocode) is:\n",
    "\n",
    "```\n",
    "ndarray[location_of_values] = new_value\n",
    "```\n",
    "\n",
    "Let's take a look at what that looks like in actual code. With our 1D array, we can specify one specific index location:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 34,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "['orange' 'blue' 'black' 'blue' 'purple']\n"
     ]
    }
   ],
   "source": [
    "a = np.array(['red','blue','black','blue','purple'])\n",
    "\n",
    "a[0] = 'orange'\n",
    "\n",
    "print(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Or we can assign multiple values at once:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 35,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "['orange' 'blue' 'black' 'pink' 'pink']\n"
     ]
    }
   ],
   "source": [
    "a[3:] = 'pink'\n",
    "\n",
    "print(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With a 2D ndarray, just like with a 1D ndarray, we can assign one specific index location:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 36,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[ 1  1  1  1  1]\n",
      " [ 1  1 99  1  1]\n",
      " [ 1  1  1  1  1]]\n"
     ]
    }
   ],
   "source": [
    "ones = np.array([[1, 1, 1, 1, 1],\n",
    "                 [1, 1, 1, 1, 1],\n",
    "                 [1, 1, 1, 1, 1]])\n",
    "\n",
    "ones[1,2] = 99\n",
    "\n",
    "print(ones)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also assign a whole row..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 37,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[42 42 42 42 42]\n",
      " [ 1  1 99  1  1]\n",
      " [ 1  1  1  1  1]]\n"
     ]
    }
   ],
   "source": [
    "ones[0] = 42\n",
    "\n",
    "print(ones)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "...or a whole column:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 38,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[42 42  0 42 42]\n",
      " [ 1  1  0  1  1]\n",
      " [ 1  1  0  1  1]]\n"
     ]
    }
   ],
   "source": [
    "ones[:,2] = 0\n",
    "\n",
    "print(ones)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Boolean arrays become very powerful when we use them for assignment. Let's look at an example:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 39,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[ 1  2 99 99 99]\n"
     ]
    }
   ],
   "source": [
    "a2 = np.array([1, 2, 3, 4, 5])\n",
    "\n",
    "a2_bool = a2 > 2\n",
    "\n",
    "a2[a2_bool] = 99\n",
    "\n",
    "print(a2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The boolean array controls the values that the assignment applies to, and the other values remain unchanged. Let's look at how this code works:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 40,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = np.array([1, 2, 3, 4, 5]) \n",
    "\n",
    "a > 2 # It returns TRUE for the 3rd, 4th and 5th indexes]\n",
    "\n",
    "a[a>2] = 99 # It assigns the new value to the  3rd, 4th and 5th elements"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice in the diagram above that we used a \"shortcut\" - we inserted the definition of the boolean array directly into the selection. This \"shortcut\" is the conventional way to write boolean indexing. Up until now, we've been assigning to an intermediate variable first so that the process is clear, but from here on, we will use this \"shortcut\" method instead."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we'll look at an example of assignment using a boolean array with two dimensions:\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 41,
   "metadata": {},
   "outputs": [],
   "source": [
    "b = np.array([\n",
    "    [1, 2, 3],\n",
    "    [4, 5, 6],\n",
    "    [7, 8, 9]\n",
    "])\n",
    "\n",
    "b[b>4] = 99"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `b > 4` boolean operation produces a 2D boolean array which then controls the values that the assignment applies to.\n",
    "\n",
    "We can also use a 1D boolean array to perform assignment on a 2D array:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 42,
   "metadata": {},
   "outputs": [],
   "source": [
    "c = np.array([\n",
    "    [1, 2, 3],\n",
    "    [4, 5, 6],\n",
    "    [7, 8, 9]\n",
    "])\n",
    "\n",
    "c[c[:,1]>2,1] = 99"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `c[:,1] > 2` boolean operation compares just one column's values and produces a 1D boolean array. We then use that boolean array as the row index for assignment, and `1` as the column index to specify the second column. Our boolean array is only applied to the second column, while all other values remaining unchanged.\n",
    "\n",
    "The pseudocode syntax for this code is as follows, first using an intermediate variable:\n",
    "\n",
    "```\n",
    "bool = array[:, column_for_comparison] == value_for_comparison\n",
    "\n",
    "array[bool, column_for_assignment] = new_value\n",
    "```\n",
    "\n",
    "and then all in one line:\n",
    "\n",
    "```\n",
    "array[array[:, column_for_comparison] == value_for_comparison, column_for_assignment] = new_value\n",
    "```"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
