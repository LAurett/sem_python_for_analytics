#!/usr/bin/env python
# coding: utf-8

# In[8]:


import format_months as fm
import pandas as pd


# In[22]:


def load_months_data(month='*'):
  
  main_url = 'https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/'

  if month == '*':
    days = fm.get_months_days_range(['02', '03', '04', '05'])
  else:
    days = fm.get_month_days_range(month)

  for i, d in enumerate(days):
    url_provinces = main_url + 'dati-province/dpc-covid19-ita-province-{}.csv'.format(d)
    url_regions = main_url +  'dati-regioni/dpc-covid19-ita-regioni-{}.csv'.format(d)
    url_country = main_url +  'dati-andamento-nazionale/dpc-covid19-ita-andamento-nazionale-{}.csv'.format(d)

    if i == 0:      
      provinces = pd.read_csv(url_provinces, error_bad_lines=False)
      regions = pd.read_csv(url_regions, error_bad_lines=False)
      country = pd.read_csv(url_country, error_bad_lines=False)
    else:
      province = pd.read_csv(url_provinces, error_bad_lines=False)
      region = pd.read_csv(url_regions, error_bad_lines=False)
      italy = pd.read_csv(url_country, error_bad_lines=False)

      provinces = pd.concat([provinces, province])
      regions = pd.concat([regions, region])
      country = pd.concat([country, italy])

  return provinces, regions, country

