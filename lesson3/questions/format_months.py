#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import format_days as fd


# In[ ]:


def build_month_days_range(month, days):     
    res = ["2020" + month + v for v in days]
    return(res) 


# In[ ]:


def get_month_days_range(month):
    days_range = fd.get_days_range(1,32)
    
    if month == '02':
        md = build_month_days_range(month, days_range[23:29]) # from 24th up to the 29th
    elif month == '03':
        md = build_month_days_range(month, days_range) # from 1st up to the 31th
    elif month == '04':
        md = build_month_days_range(month, days_range[0:30]) # from 1st up to the 30th
    elif month == '05':
        md = build_month_days_range(month, days_range[0:12]) # from 1st up to the 12th

    return(md)


# In[ ]:


def get_months_days_range(months):        
    months_days = []
    
    for m in months:
       md = get_month_days_range(m)
       months_days = months_days + md
        
    return(months_days)

